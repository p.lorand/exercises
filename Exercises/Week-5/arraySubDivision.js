// https://www.hackerrank.com/challenges/the-birthday-bar/problem

function calculatePieces(s, d, m, index, pieces) {
  let index2 = index;
  let pieces2 = pieces;
  let arr = [...s];

  if (index2 === s.length) {
    return pieces2;
  }

  const segment = arr.splice(index, m);
  const sum = segment.reduce((acc, value) => {
    return acc + value;
  }, 0);

  if (sum === d) {
    pieces2 += 1;
  }

  index2 += 1;
  return calculatePieces(s, d, m, index2, pieces2);
}

function birthday(s, d, m) {
  return calculatePieces(s, d, m, 0, 0);
}

const arr = [1, 2, 1, 3, 2];
const arr2 = [1, 1, 1, 1, 1, 1];
const arr3 = [4];
const arr4 = [2, 5, 1, 3, 4, 4, 3, 5, 1, 1, 2, 1, 4, 1, 3, 3, 4, 2, 1];
const arr5 = [
  2,
  2,
  2,
  1,
  3,
  2,
  2,
  3,
  3,
  1,
  4,
  1,
  3,
  2,
  2,
  1,
  2,
  2,
  4,
  2,
  2,
  3,
  5,
  3,
  4,
  3,
  2,
  1,
  4,
  5,
  4
];
console.log(birthday(arr5, 10, 4));
