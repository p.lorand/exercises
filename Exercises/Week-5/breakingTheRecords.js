// https://www.hackerrank.com/challenges/breaking-best-and-worst-records/problem

function breakingRecords(arr) {
  const performance = arr.reduce(
    (acc, value) => {
      let highetPerformance = acc[0];
      let lowestperformance = acc[1];
      if (highetPerformance[highetPerformance.length - 1] < value) {
        highetPerformance.push(value);
      } else if (lowestperformance[lowestperformance.length - 1] > value) {
        lowestperformance.push(value);
      }
      return [highetPerformance, lowestperformance];
    },
    [[arr[0]], [arr[0]]]
  );
  performance[0].shift();
  performance[1].shift();
  return [performance[0].length, performance[1].length];
}

const arr = [10, 5, 20, 20, 4, 5, 2, 25, 1];
const arr2 = [3, 4, 21, 36, 10, 28, 35, 5, 24, 42];
console.log(breakingRecords(arr2));
