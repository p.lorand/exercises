// https://www.hackerrank.com/challenges/reduced-string/problem

function reduce(s, index) {
  if (s.length === 0) {
    return "Empty String";
  }
  if (s.length === index) {
    return s;
  }
  let index2 = index;
  if (s.charAt(index) === s.charAt(index - 1)) {
    s = s.substring(0, index - 1) + s.substring(index + 1);
    index2 = 0;
    return reduce(s, index2);
  }
  index2 += 1;
  return reduce(s, index2);
}

function superReducedString(s) {
  return reduce(s, 1);
}

// function reduce(s, prevReduced, length, index) {
//   const regex = /(.)\1/g;
//   let reduced = s.replace(regex, "") || "Empty String";
//   let index2 = index;
//   if (index2 === length) {
//     return prevReduced;
//   }
//   // console.log("prevReduced - ", prevReduced);
//   // console.log("Reduced - ", reduced);
//   if (prevReduced === reduced) {
//     reduced = prevReduced.replace(regex, "") || "Empty String";
//   }
//   index2 += 1;
//   // console.log("Reduced 2 - ", reduced);
//   return reduce(s, reduced, length, index2);
// }

const str1 =
  "acdqglrfkqyuqfjkxyqvnrtysfrzrmzlygfveulqfpdbhlqdqrrqdqlhbdpfqluevfgylzmrzrfsytrnvqyxkjfquyqkfrlacdqj";
const str2 = "aaabccddd";
const str3 = "aa";
const str4 = "baab";
console.log(superReducedString(str1));
