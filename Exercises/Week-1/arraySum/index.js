function parseValue(value) {
  const parsed = parseInt(value, 10);
  return isNaN(parsed) ? 0 : parsed
}

function arraySum(array) {
    if (!array.length) {
        return null;
    }

    const sum = array.reduce((acc, currentValue) => {
      const value = parseValue(currentValue);
      return acc + value
      });
    return sum;
}