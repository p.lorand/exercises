
function compareArrays(array1, array2) {
  if (!array1.length || !array2.length) {
      return null;
  }

  let sum = [0, 0];

  if (array1.length === array2.length) {
    const sum2 = array1.map((item, index) => {

      if (item > array2[index]) {
          return sum[0]++
        } else if (item < array2[index]) {
          return sum[1]++
        }
        return sum
      });
    return sum;
    }
  return [];
}

// Remake with reduce
function compareArrays2(array1, array2) {
  if (array1.length === array2.length) {
    return array1.reduce(
      (acc, item, index) => {
        if (item > array2[index]) {
          return [acc[0] + 1, acc[1]];
        } else if (item < array2[index]) {
          return [acc[0], acc[1] + 1];
        }
        return acc;
      },
      [0, 0]
    );
  }
}
