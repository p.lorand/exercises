// Return the index of the character that needs to be modified in order to make the string a palindrom


// function palindromeIndex2(s = "") {
//   const splitted = s.split("");
//   if ([...splitted].reverse().join("") === s) {
//     return -1;
//   }
//   return splitted.reduce((acc, value, index, text) => {
//     if (acc > -1) {
//       return acc;
//     }
//     // TODO: incearca cu for pana la jumatate si cu comaparitie in oglinda si scoti cate unu
//     // reduce cautarea 
//     if (index < text.length / 2) {
//       const temp = [...text];
//       temp.splice(index, 1);
//       if (temp.join("") === temp.reverse().join("")) {
//         return index;
//       }
//       return acc;
//     }
//   }, -1);
// }


// function palindromeIndex1(s = "") {
//   const stringLength = s.length;
//   const splittedString = s.split("");
//   if (
//     stringLength < 1 &&
//     stringLength > 100005 &&
//     s !== splittedString.reverse().join("")
//   ) {
//     return -1;
//   }
//   const result = splittedString.reduce(
//     (acc, value, index) => {
//       if (index < s.length / 2) {
//         return acc;
//       }
//       if (index < acc.lastIndex) {
//         if (s.charAt(index) !== s.charAt(acc.lastIndex - 1 - index)) {
//           const beginning = s.substring(0, index) + s.substring(index + 1);
//           const end =
//             s.substring(0, acc.lastIndex - 1 - index) +
//             s.substring(acc.lastIndex - 1 - index + 1);

//           // Compare both strings
//           if (beginning === beginning.split("").reverse().join("")) {
//             return {
//               ...acc,
//               indexToChange: index,
//             };
//           } else if (end === end.split("").reverse().join("")) {
//             return {
//               ...acc,
//               indexToChange: acc.lastIndex - 1 - index,
//             };
//           }
//         }
//       }
//       return {
//         ...acc,
//       };
//     },
//     {
//       indexToChange: -1,
//       lastIndex: s.length,
//     }
//   );
//   return result.indexToChange;
}

function isPalindrome(s) {
  for (let i = 0, j = s.length - 1; i < Math.floor(s.length / 2); i++, j--) {
    if (s.charAt(i) !== s.charAt(j)) {
      return false;
    }
  }
  return true;
}

function palindromeIndex(s) {
  if (isPalindrome(s)) {
    return -1;
  }

  /**
   * Mentii pointerele catre inceput si capatul string-ului.
   * Verifici in oglinda fiecare caracter.
   * Daca nu sunt egale atunci returnezi un substring excluzand ultimul caracter pe care l-ai verificat s.substring(i, j)
   * Verifici daca acel substring este palindrom
   * Daca este palindrom atunci inseamna ca caracterul de la capat trebuie schimbat deci returnezi j
   * Daca nu este palindrom atunci inseamna ca trebuie ca indexul de la inceput trebuie schimbat
   */
  for (let i = 0, j = s.length - 1; i < Math.floor(s.length / 2); i++, j--) {
    if (s.charAt(i) !== s.charAt(j)) {
      if (isPalindrome(s.substring(i, j))) {
        return j;
      } else {
        return i;
      }
    }
  }
}

const test1 = "aaab"; // index 3
const test2 = "baa"; // index 0
const test3 = "aaa"; // -1
const test4 = "a"; // -1
const test5 = "acbba"; // 1
console.log(palindromeIndex(test4));
