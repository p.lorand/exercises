// https://www.hackerrank.com/challenges/the-love-letter-mystery/problem

// Second way of solving
function recursive(text = "", textStart, textEnd, count) {
  if (textStart >= textEnd) {
    return count;
  }
  const textStartCharcode = text[textStart].charCodeAt(0) - 96;
  const textEndCharcode = text[textEnd].charCodeAt(0) - 96;
  count += Math.abs(textStartCharcode - textEndCharcode);
  return recursive(text, textStart + 1, textEnd - 1, count);
}

function theLoveLetterMystery(s = "") {
  const splittedString = s.split("");
  const result = splittedString.reduce(
    (acc, value, index) => {
      if (index < acc.lastIndex) {
        const stringStartCharCode = s[index].charCodeAt(0) - 96;
        const stringEndCharCode = s[acc.lastIndex].charCodeAt(0) - 96;
        return {
          count: acc.count + Math.abs(stringStartCharCode - stringEndCharCode),
          lastIndex: acc.lastIndex - 1
        };
      }
      return acc;
    },
    {
      count: 0,
      lastIndex: s.length - 1
    }
  );
  return result.count;
  // return recursive(s, 0, s.length - 1, 0);
}

const test1 = "abcd"; // 4
const test2 = "abc"; // 2
const test3 = "abcba"; // 0
const test4 = "abcd"; // 2

console.log(theLoveLetterMystery("abcd"));
