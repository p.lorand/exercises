// https://www.hackerrank.com/challenges/beautiful-binary-string/problem

function beautifulBinaryString(b = "") {
  const regex = /010/g;
  const result = b.match(regex);

  if (result == null) {
    return 0;
  }
  return result.length;
}

const test1 = '0100101010'; // 3
const test2 = '01100'; // 0
const test3 = '0101010'; // 2

console.log(beautifulBinaryString(test1));