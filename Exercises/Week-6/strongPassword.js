// https://www.hackerrank.com/challenges/strong-password/problem

function minimumNumber(n, password) {
  let count = 0;
  const numbersPattern = /[0-9]/;
  const uppercasePatter = /[A-Z]/g;
  const lowerCasePatter = /[a-z]/g;
  const specialCharacters = /[!@#$%^&*()\-+"]/;
  // How can we concatenate more regex into one?
  
  if (!password.match(numbersPattern)) {
    console.log("1");
    count++;
  }
  if (!password.match(uppercasePatter)) {
    console.log("2");
    count++;
  }
  if (!password.match(lowerCasePatter)) {
    console.log("3");
    count++;
  }
  if (!password.match(specialCharacters)) {
    console.log("4");
    count++;
  }
  return Math.max(count, 6 - n);
}

const pw = "4700"; // res - 3
// const pw2 = "#HackerRank"; // res - 1
console.log(minimumNumber(4, pw));
