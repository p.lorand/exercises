// https://www.hackerrank.com/challenges/caesar-cipher-1/problem

function caesarCipher(s, k) {
  const alphabetLowercase = "abcdefghijklmnopqrstuvwxyz";
  const alphabetUppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  const stringSplitted = s.split("");
  let finalString = stringSplitted;

  for (let index = 0; index < stringSplitted.length; index++) {
    const char = s.charAt(index);
    if (char.match(/[a-z]/g)) {
      finalString[index] = alphabetLowercase.charAt(
        (alphabetLowercase.indexOf(char) + k) % 26
      );
    } else if (char.match(/[A-Z]/g)) {
      finalString[index] = alphabetUppercase.charAt(
        (alphabetUppercase.indexOf(char) + k) % 26
      );
    } else {
      finalString[index] = char;
    }
  }
  return finalString.join("");
}

const s = "middle-Outz"; // k =2 => okffng-Qwvb 
// const s = "www.abc.xy"; // k = 87 => fff.jkl.gh 
console.log(caesarCipher(s, 2));