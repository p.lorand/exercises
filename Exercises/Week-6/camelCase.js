// https://www.hackerrank.com/challenges/camelcase/problem

function camelcase(s) {
  const uppercaseLetters = s.length - s.replace(/[A-Z]/g, "").length;
  if (s.charAt(0) === s.match(/[a-z]/g, "")[0]) {
    return uppercaseLetters + 1;
  }
  return uppercaseLetters;
}

const s = "saveChangesInTheEditor";

console.log(camelcase(s));
