// https://www.hackerrank.com/challenges/game-of-thrones/problem

function gameOfThrones(s = "") {
  const splitted = s.split("").sort();
  /**
   * A palindrom is formed by having couples of characters
   
   * We sort the string
   * Loop trough it in order to find the characters that do not have a couple
   * Have two counters, one simple and one for odd number of appearances
   * Do this by counting each letter and if a letter appeard an odd time then increase a counter
   * Make the somple counter 1 again in order to be ready to count the next charaters
   * 
   * We have a palindrom if we have at least one odd character cpount
   */
  const result = splitted.reduce(
    (acc, value, index) => {
      console.log("Reduce - ", value, index, acc.counter);
      if (value === splitted[index + 1]) {
        return {
          ...acc,
          counter: acc.counter + 1,
        };
      } else {
        if (acc.counter % 2 === 1) {
          return {
            ...acc,
            oddCounter: acc.oddCounter + 1,
          };
        }
        return {
          ...acc,
          counter: 1,
        };
      }
    },
    {
      counter: 1,
      oddCounter: 0,
    }
  );
  return result.oddCounter > 1 ? "NO" : "YES";
}

const test1 = "cdcdcdcdeeeef";
const test2 = "cdefghmnopqrstuvw";
const test3 = "aaabbbb";

console.log(gameOfThrones(test3));
