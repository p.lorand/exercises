// https://www.hackerrank.com/challenges/two-strings/problem

function twoStrings(s1, s2) {
  const splitted = s1.split("");
  return splitted.reduce((acc, value) => {
    if (s2.includes(value)) {
      return "YES";
    }
    return acc;
  }, "NO");
}

const s1 = "hello";
const s2 = "world";

const s3 = "hi"; 
const s4 = "world";
console.log(twoStrings(s3, s4));