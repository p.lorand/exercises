/**
 * In this challenge, you are required to calculate and print the sum of the elements in an array
 * keeping in mind that some of those integers may be quite large.
 */

const addBigNumbers2 = (v1, v2) => {
  const decV1 = String(v1).split("").reverse();
  const decV2 = String(v2).split("").reverse();
  const total = [];

  let extra = 0;
  const maxLength = decV1.length > decV2.length ? decV1.length : decV2.length;
  console.log(decV1);
  for (let i = 0; i < maxLength; i++) {
    const d1 = +(decV1[i] ?? 0);
    const d2 = +(decV2[i] ?? 0);

    const sum = d1 + d2 + extra;
    if (sum > 9) {
      total.push(sum - 10);
      extra = 1;
    } else {
      total.push(sum);
      extra = 0;
    }
  }
  return total.reverse().join("");
};


const recursive = (arr, acc, v2, index) => {
  let index2 = index;
  let total = acc;
  const decV1 = String(total).split().reverse();
  const decV2 = String(v2).split().reverse();
  const d1 = +(decV1[0] ?? 0);
  const d2 = +(decV2[0] ?? 0);

  const sum = d1 + d2;
  if (sum > 9) {
    total = +(sum - 10);
  } else {
    total = +sum;
  }
  if (index2 < arr.length - 1) {
    index2 += 1;
    return recursive(arr, total, arr[index2], index2);
  }

  return total;
};

function aVeryBigSum(arr) {
  return arr.reduce((acc, currentValue) => {
    return BigInt(acc) + BigInt(currentValue);
  });
}

const array = [1000000001, 1000000002, 1000000003, 1000000004, 1000000005];
console.log(aVeryBigSum(array));
