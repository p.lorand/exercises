/**
  Its base and height are both equal to . It is drawn using # symbols and spaces. The last line is not preceded by any spaces.
  Write a program that prints a staircase of size 

        #
       ##
      ###
     #### 
 */


function staircase(n) {
  let matrix = Array(n)
    .fill()
    .map(() => Array(n).fill(" "));

  matrix.forEach((array, index) => {
    array.fill("#", array.length - ++index, array.length);
  });
  console.log(matrix);
}

// Remake staircase with a different solution
function staircase2(n) {
  return Array(n)
    .fill()
    .map((v, index) => [...Array(n - index - 1).fill(" "), ...Array(index + 1).fill("#")].join(''))
    .join('\n');
}

console.log(staircase(6));