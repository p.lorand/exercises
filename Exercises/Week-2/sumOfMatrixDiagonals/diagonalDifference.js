/**
 * Given a square matrix, calculate the absolute difference between the sums of its diagonals.
 */

function diagonalDifference(arr) {
  let accumulator1 = 0;
  let accumulator2 = 0;
  arr.forEach((currentValue, index) => {
    accumulator1 += currentValue[index];
    accumulator2 += currentValue[(currentValue.length - 1) - index];
  });
  return Math.abs(accumulator1 - accumulator2);
}

// Remake using reduce
function diagonalDifference(arr) {
  return arr.reduce((acc, currentValue, index) => {
    return acc + currentValue[currentValue.length - 1 - index] - currentValue[index];
  }, 0);
}

const array = [
  [11, 2, 4],
  [4, 5, 6],
  [10, 8, -12]
];
console.log(diagonalDifference2(array));