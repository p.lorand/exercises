
// TODO: refa-l si adauga fiecare caracted intr-un obiect cu o valoare, incrementeaza aloarea cand il mai gasesti 
function gemstones(arr) {
  const array = [...arr];
  let firstItem = array.shift();
  const splitted = firstItem.split("");
  const results = splitted.reduce(
    (acc1, value1) => {

      const gems = array.reduce((acc2, value2) => {
        if (value2.match([value1])) {
          return acc2 + 1;
        }
        return acc2;
      }, 0);

      if (gems === array.length) {
        return {
          ...acc1,
          gemStones: acc1.gemStones + 1
        };
      }
      return acc1;
    },
    {
      firstItem,
      gemStones: 0
    }
  );
  return results.gemStones;
}

// https://www.hackerrank.com/challenges/gem-stones/problem

function gemstones(arr) {
  const s = arr.reduce((a, c) => {
    return new Set(
      [...a].filter((x) => {
        return [...c].includes(x);
      })
    );
  });
  return [...s].length;
}

const arr = ["abcdde", "baccd", "eeabg"];
console.log(gemstones(arr));
