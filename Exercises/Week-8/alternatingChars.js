// https://www.hackerrank.com/challenges/alternating-characters/problem

function alternatingCharacters(s) {
  // TODO: modify to return count
  [...s].reduce(
    (acc, value) => {
      if (acc.value === value) {
        return {
          value,
          count: acc.count + 1,
        };
      }
      return {
        ...acc,
        value
      };
    },
    {
      value: [...s][0],
      count: 0,
    }
  );
  return count;
}
const s = "AABAAB"; // => 2
console.log(alternatingCharacters(s));
