
function funnyString(s) {
  const splitted = s.split("");
  for (let index = 0; index < Math.round(splitted.length / 2); index++) {
    if (
      Math.abs(s.charCodeAt(index) - s.charCodeAt(index + 1)) !==
      Math.abs(
        s.charCodeAt(splitted.length - 1 - index) -
          s.charCodeAt(splitted.length - 1 - (index + 1))
      )
    ) {
      return "Not Funny";
    }
  }
  return "Funny";
}

const funny = "acxz";
const funny2 = "jkotzxzxrxtzytlruwrxytyzsuzytwyzxuzytryzuzysxvsmupouysywywqlhg";

const notFunny = "bcxz";
const notFunny2 = "ivvkxq";
const notFunny3 = "ivvkx";
console.log(funnyString(funny2));
