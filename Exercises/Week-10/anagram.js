// https://www.hackerrank.com/challenges/anagram/problem

/**
 * Count how many times the first string does not include characters from the second string
 */

function anagram(s) {
  const stringLength = s.length;
  if (stringLength % 2 === 1) {
    return -1;
  }
  let stringStart = s.substring(0, stringLength / 2);
  let stringEnd = s.substring(stringLength / 2, stringLength);
  let count = 0;
  stringEnd
    .split("")
    .filter((value) =>
      stringStart.includes(value)
        ? (stringStart = stringStart.replace(value, ""))
        : count++
    );
  return count;
}

const test1 = "aaabbb"; //3
const test2 = "xaxbbbxx"; //1
const test3 = "xyyx"; //0
const test4 = "abc"; // -1

console.log(anagram(test4));



// https://www.hackerrank.com/challenges/anagram/problem

/**
 * Count how many times the first string does not include characters from the second string
 */

function anagram(s) {
  const stringLength = s.length;
  if (stringLength % 2 === 1) {
    return -1;
  }
  let stringStart = s.substring(0, stringLength / 2);
  let stringEnd = s.substring(stringLength / 2, stringLength);
  let count = 0;
  // Remake with reduce, folosete replace, in acc un string si un count
  stringEnd
    .split("")
    .filter((value) =>
      stringStart.includes(value)
        ? (stringStart = stringStart.replace(value, ""))
        : count++
    );
  return count;
}

const test1 = "aaabbb"; //3
const test2 = "xaxbbbxx"; //1
const test3 = "xyyx"; //0
const test4 = "abc"; // -1

console.log(anagram(test1));

