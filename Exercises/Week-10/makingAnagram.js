// https://www.hackerrank.com/challenges/making-anagrams/problem

/**
 * Remove every character that is found in both strings
 * Return the length of the two strings
 */
function makingAnagrams(s1, s2) {
  let string1 = s1;
  let string2 = s2;

  string1.split("").forEach((element) => {
    if (string2.includes(element)) {
      string1 = string1.replace(element, "");
      string2 = string2.replace(element, "");
    }
  });
  return string1.length + string2.length;
}

const s1 = "cde";
const s2 = "abc"; // 4

console.log(makingAnagrams(s1, s2));
