// https://www.hackerrank.com/challenges/time-conversion/problem

function getPM(time, hourParsed) {
  if (hourParsed === 12) {
    return `${hourParsed}${time.substring(2, time.length - 2)}`;
  }
  return `${hourParsed + 12}${time.substring(2, time.length - 2)}`;
}

function getAM(time, hourParsed) {
  if (hourParsed.toString().length === 2) {
    const hour = hourParsed === 12 ? "00" : `${hourParsed}`;
    return `${hour}${time.substring(2, time.length - 2)}`;
  }
  return `0${hourParsed}${time.substring(2, time.length - 2)}`;
}

function timeConversion(s) {
  const firstTwoDigits = s.substring(0, 2);
  const hourParsed = parseInt(firstTwoDigits, 10);
  if (s.match(/PM/)) {
    return getPM(s, hourParsed);
  } else if (s.match(/AM/)) {
    return getAM(s, hourParsed);
  }
}

const time = "12:45:54PM";
console.log(timeConversion(time));
