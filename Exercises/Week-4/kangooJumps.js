function recursive(x1, v1, x1Jumps, x2, v2, x2Jumps, index) {
  if (x1Jumps === x2Jumps && x1 === x2) {
    return "YES";
  }

  if (x2 > x1 && v2 > v1) {
    return "NO";
  }

  if (v1 === v2) {
    return "NO";
  }

  if (index === 1000) {
    return "NO";
  }

  let x1NewPosition = x1;
  let x1NewJumps = x1Jumps;
  let x2NewPosition = x2;
  let x2NewJumps = x2Jumps;
  let newIndex = index;

  if (x1 === index) {
    x1NewPosition += v1;
    x1NewJumps += 1;
  }
  if (x2 === index) {
    x2NewPosition += v2;
    x2NewJumps += 1;
  }
  newIndex += 1;
  return recursive(
    x1NewPosition,
    v1,
    x1NewJumps,
    x2NewPosition,
    v2,
    x2NewJumps,
    newIndex
  );
}

function kangaroo(x1, v1, x2, v2) {
  if (x2 > x1 && v2 > v1) {
    return "NO";
  }
  return recursive(x1, v1, 0, x2, v2, 0, 0);
}

console.log(kangaroo(21, 6, 47, 3));
