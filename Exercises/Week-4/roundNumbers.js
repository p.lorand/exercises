// https://www.hackerrank.com/challenges/grading/problem

function gradingStudents(grades) {
  return grades.map((value) => {
    const diffToNextMultiple = 5 - (value % 5);
    if (value < 38) {
      return value;
    }
    return diffToNextMultiple < 3 ? value + diffToNextMultiple : value;
  });
}

const arr = [73, 67, 38, 33];
console.log(gradingStudents(arr));
