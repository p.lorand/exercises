// https://www.hackerrank.com/challenges/pangrams/problem

function pangrams(s) {
  const regex = /([a-z])(?!.*\1)/g;
  const val = s.toLowerCase();

  if (val.match(regex).length === 26) {
    return "pangram";
  }
  return "not pangram";
}

const pangram = "We promptly judged antique ivory buckles for the next prize";
const notPangram = "We promptly judged antique ivory buckles for the prize"

console.log(pangrams(pangram));