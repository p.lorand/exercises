// https://www.hackerrank.com/challenges/mars-exploration/problem

function recursive(s, index, substringStart, substringEnd, difference) {
  const sos = "SOS";
  if (s === sos) {
    return 0;
  }
  const splitted = s.split("");
  // TODO: conditia de iesire sa fie empty string
  // substring start si end va fi mereu 0 si 3
  if (index < splitted.length / 3) {
    const segment = s.substring(substringStart, substringEnd);
    if (segment.charAt(0) !== "S") {
      difference++;
    }
    if (segment.charAt(1) !== "O") {
      difference++;
    }
    if (segment.charAt(2) !== "S") {
      difference++;
    }
    return recursive(
      s,
      index + 1,
      substringStart + 3,
      substringEnd + 3,
      difference
    );
  }
  return difference;
}

// Complete the marsExploration function below.
function marsExploration(s) {
  return recursive(s, 0, 0, 3, 0);
}

const s = "SOSSOT";

console.log(marsExploration(s));
