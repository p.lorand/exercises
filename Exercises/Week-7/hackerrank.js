// https://www.hackerrank.com/challenges/hackerrank-in-a-string/problem?h_r=next-challenge&h_v=zen

// Complete the hackerrankInString function below.
function hackerrankInString(s) {
  // Can this be made using Regex?
  const hackerrank = "hackerrank";
  const hackerrankSplitted = hackerrank.split("");
  const splitted = s.split("");
  if (splitted.length < hackerrankSplitted.length) {
    return "NO";
  }
  const result = splitted.reduce((acc, value) => {
    if (acc < hackerrankSplitted.length && value === hackerrank.charAt(acc)) {
      return acc + 1;
    }
    return acc;
  }, 0);
  return result === hackerrankSplitted.length ? "YES" : "NO";
}

const s = "hackerworld";

console.log(hackerrankInString(s));
