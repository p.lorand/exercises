/*
 Return the occurances of the highest number from an array
 https://www.hackerrank.com/challenges/birthday-cake-candles/problem?h_r=next-challenge&h_v=zen&h_r=next-challenge&h_v=zen
*/

function birthdayCakeCandles(candles) {
  const biggestNumber = Math.max(...candles);
  return candles.reduce((acc, value) => {
    return value === biggestNumber ? (acc += 1) : acc;
  }, 0);
}

// Remake with only one iteration
function birthdayCakeCandles2(candles) {
  return candles.reduce(
    (acc, value) => {
      if (acc.maxValue < value) {
        return {
          maxValue: value,
          count: 1
        };
      } else if (acc.maxValue === value) {
        return {
          ...acc,
          count: acc.count + 1
        };
      }
      return acc
    },
    {
      maxValue: 0,
      count: 0
    }
  ).count;
}

const arr = [3, 2, 1, 3, 3, 3];
console.log(birthdayCakeCandles(arr));
// Expected result: 4
