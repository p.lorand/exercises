// https://www.hackerrank.com/challenges/mini-max-sum/problem?h_r=next-challenge&h_v=zen

function getSumOfEachValue(arr) {
  let sumOfEachValue = [];

  arr.forEach((element, index) => {
    const filteredElements = arr.filter(
      (value, filterIndex) => filterIndex !== index
    );
    const sum = filteredElements.reduce((acc, value) => {
      return acc + value;
    }, 0);
    sumOfEachValue.push(sum);
  });
  return sumOfEachValue;
}

function miniMaxSum(arr) {
  let finalNumbers = new Array(2);
  const sumOfEachValue = getSumOfEachValue(arr);
  console.log(sumOfEachValue);

  finalNumbers[0] = Math.min(...sumOfEachValue);
  finalNumbers[1] = Math.max(...sumOfEachValue);
  return finalNumbers.join(" ");
}

const arr = [1, 2, 3, 4, 5];

console.log(miniMaxSum(arr));
