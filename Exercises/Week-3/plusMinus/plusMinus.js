/**
 * https://www.hackerrank.com/challenges/plus-minus/problem
 */

function calculateProportion(value, totalValues) {
  const proportion = value / totalValues;
  return proportion.toFixed(6);
}

function sumOfEachValueType(arr) {
  let sum = {
    positive: 0,
    negateive: 0,
    zero: 0
  };
  arr.forEach((element) => {
    if (element > 0) {
      sum.positive += 1;
    } else if (element < 0) {
      sum.negateive += 1;
    } else if (element === 0) {
      sum.zero += 1;
    }
  });
  return sum;
}

// Function remake using reduce
function sumOfEachValueType2(arr) {
  return arr.reduce(
    (acc, value) => {
      if (value > 0) {
        return {
          ...acc,
          positive: acc.positive + 1
        };
      } else if (value < 0) {
        return {
          ...acc,
          negative: acc.negative + 1
        };
      }
      return {
        ...acc,
        zero: acc.zero + 1
      };
    },
    {
      positive: 0,
      negative: 0,
      zero: 0
    }
  );
}

function plusMinus(arr) {
  let proportions = new Array(3);
  const sumValues = sumOfEachValueType(arr);

  proportions[0] = calculateProportion(sumValues.positive, arr.length);
  proportions[1] = calculateProportion(sumValues.negateive, arr.length);
  proportions[2] = calculateProportion(sumValues.zero, arr.length);
  console.log(proportions.join("\n"));
}