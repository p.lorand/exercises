### New things learned


28.MAI.21
UUID4 - if you want to generate uuid4 id-s use this library https://github.com/uuidjs/uuid#readme 

28.MAI.21
Hashes - are strings that are generated upon another string.
Deci tu practic ii dai un string si el iti genereaza un alt string de 64 de caractere in locul ei.
Daca aplicatia ta se rerandeaza sau se reporneste atunci nu se creeaza un alt hash ci tot acela.


27.MAI.21
SQLite - the format of dates and time that it supports https://www.sqlite.org/lang_datefunc.html 
You need to put a % before the date formats like: %d, %m, %s, etc.

25.MAI.21
Date 
Z de la final înseamnă universal care e convertit la time-ul unde este apelat. 2021-05-24T00:00:00.000Z

DO NOT COMPARE DATE WITH STRING because


19.MAI.21
Hash - a low level performance library that generates hashes https://github.com/h2non/jshashes#readme 

17.MAI.21
Idempotent - When performing an action gives the same result 

12.MAI.21
Sentry - auth.token is not necessary to be bundled in app by inserting it into sentry.properties.
https://github.com/getsentry/sentry-react-native/issues/112 


11.MAI.21
Navigation - If you want to navigate to a screen that is not present in your current navigation stack then you need to initialize that screen or the stack that screen lives in. Ex.:
Instead of 


navigation.navigate('WatchVideo', { ...payload })
You have
navigation.navigate('Therapy', {
   screen: 'WatchVideo',
   params: { ...payload },
   initial: false
 });


02.MAI.21
https://blog.logrocket.com/how-javascript-closures-work-in-plain-english/

28.APR.2021
Testing:
Implementation dependent: Atunci cand in test pun direct componenta pe care ma astept sa o testez.
Implementation agnostic: E la fel cum am testat pana acuma




23.APR.2021
Date - Mereu afisam local tim, cand trimitem la server trimitem ca si UTC

22.APR.2021
Jest
transformIgnorePatterns - modules in your node_modules are not transpiled by default so adding them transpiles them for jest.

16.APR.2021
Jest - in order to mock components use components from React, for example. 
jest.mock('@gorhom/bottom-sheet', () => {
 // eslint-disable-next-line global-require
 const react = require('react-native');
 
 return {
   BottomSheetModalProvider: react.View,
   BottomSheetModal: react.Modal
 };
});


15.APR.2021
Steps to take in order to rename a RN project:
rename all "ubreathe" occurrences to "nuumi"
create new bundle id and new apps in stores
generate new provisioning profiles on iOS
rename apps in appcenter
generate and upload new builds


13.APR.2021
In order to calculate the percentage of two numbers:
Let's say you have two numbers, 40 and 30.  
  30/40*100 = 75.
  So 30 is 75% of 40.  

  40/30*100 = 133. 
  So 40 is 133% of 30. 

The percentage increase from 30 to 40 is:  
  (40-30)/30 * 100 = 33%  

The percentage decrease from 40 to 30 is:
  (40-30)/40 * 100 = 25%. 

These calculations hold true whatever your two numbers.


06.APR.2021
When you start developing do not start by creating a function by first making it performant, make it simple, make it work first and only if you encounter issues with it then start solving that.

25.MAR.2021
Pași pentru dev:
Don't rush 
Make it work, you have all functionality working, no coding standard, doesn’t matter how it looks. Pui totul in acelasi fisier
If the component is more complex you make it pretty codul, il modularizezi, clean up functionality, fa hook-uri daca trebuie, muta in global 
Visual, incepi sa creezi UI-ul
Optional: daca simt dupa tot ca agata UI-ul sau orice problema de performanta exista I try to make it fast
Make it work, make it pretty (functionalitate then you make the UI pretty (modularizare, componente)), optional → optimize, don't rush 

Styling: Componenta nu ar trebui sa aiba styling legat de unde se afla pe screen, inauntrul ei poate sa aiba styling dar inafara nu trebuie sa aiba fiind ca aia o sa fie controlat apoi de cel care o foloseste.

Dozarea efortului:
Nu iti lasi tichetul cel mai challenging pe ultimele zile din sprint.
La inceputul sprint-ului prioritizam ceeeace ne aduce mai multa valoare. Valoarea e ceeace aduce un feature core-ului 
Concentreazate pe cum construiesti un feature

23.MAR.2021
Lottie helps improve the app performance and also keep the app size low. If you would use a .mp4 file to render the animation then it would take several MB while lottie uses JSON files to render the animation which take up somewhere around KB.

22.MAR.2021
You can add plurals with react-i18next.
Plurals are words like minute and minutes.
Ex:
"minutes_duration": "{{count}} minute",
 "minutes_duration_plural": "{{count}} minutes"
Usage 
t('minutes_duration', {
   count: asMinutesDuration(duration)
 });


16.MAR.2021
useEffect vs useLayoutEffect
99% of the time you want to use useEffect

useLayoutEffect
It runs right before React renders (paints) everything on the screen but after 

useEffect 
Runs after React renders the components



15.MAR.2021
useState vs useReducer 
Use useReducer when you have an element of state that is dependent on another element from the state
Use useState in any other situations

12.MAR.2021
FlatList performance optimization
In cazul in care ai de randat mai multe widgets pe un screen atunci 
Nu modifici la wright!
De exemplu atunci cand vin date din saga, nu le modifici in reducer
De aceea avem multe utils 
Async storage vs Encrypted (aici pui doar chestii sensibile gen tokens)
In async storage o sa tinem informatiile generale
In Encrypted async storage tinem informatii precum token-ul fiind ca Encrypt-ul ruleaza mai incet fiind ca face encript si decript 

5.MAR.2021
Async Storage is great but it lacks security. There is a library that we can use to encrypt the async storage in react-native.
https://www.npmjs.com/package/react-native-encrypted-storage

4.MAR.2021
In order to remove extra text padding on android add includeFontPadding={false} to text component

25.FEB.2021
When you create a PR and you have github actions that run some tests then what happenes is that github makes a clone of your project, installs all the dependencies and then it runs the tests. 
This way those tests will start taking up more and more time until they finish so a good idea would be to cache the modules so they should not be installed every time at each PR. 
This is how you cache:

- name: Get yarn cache directory path








       id: yarn-cache-dir-path




       run: echo "::set-output name=dir::$(yarn cache dir)"










     - name: Restore yarn cache




       uses: actions/cache@v2




       id: yarn-cache # use this to check for `cache-hit` (`steps.yarn-cache.outputs.cache-hit != 'true'`)




       with:




         path: ${{ steps.yarn-cache-dir-path.outputs.dir }}




         key: ${{ runner.os }}-yarn-${{ hashFiles('**/yarn.lock') }}




         restore-keys: |




           ${{ runner.os }}-yarn-
run: yarn --prefer-offline



22.FEB.2021
Testing library / jest: In order to test a function callback you need to:
 mock the onPress 
Take it from the setup const { queryAllByTestId, onChange } = setup()
Fire an event to that component
Expect onChange toHaveBeenCalledWith(props you want to test)
expect(onChange).toHaveBeenCalledWith({ id: 222, label: 'Total' }, 2);

In order to test a nested component then you need to use the queryAllByTestId
const thirdSegment = queryAllByTestId('segmentedControlButton')[2];



19.FEB.2021
Testing library
If you have a segmented control, you will have an array of components rendered. How you test one of the components in the array is by receiving the entire array first using queryAllByTestId and you do something like this: queryAllByTestId('segmentedControlButton')[0] → this will return the first component in the array (segment)

Jest
If you have a prop and you want to match your object towards that prop which also is an object than you can use toMatchObject


18.FEB.2021
CI/CD - GitHub, GitLab, etc

Suspense - is a new tool for React added in 2020. It allows you to render a component while a call is pending or it errored.Ï

17.FEB.2021
If you directly modify a variable that comes from a function parameter that means that JS has sent a copy of that variable so you can modify it and the function remains pure.
function amtPrimitive(amt) {
 amt = amt + amt * 6;
 return amt;
}
const amt = 3;
console.log(amtPrimitive(amt)); // 21
console.log(amt); // 3

If the value received in the parameter is an object that that is sent by JS as reference in which case you will also modify the outside object
function amtOBJ(amt) {
 amt.value = amt.value + amt.value * 6;
 return amt;
}
const amount = { value: 3 };
console.log(calculateFinalPurchaseAmount(amount)); // value: 21
console.log(amount); // value: 21


17.FEB.2021
Setting the state in React is not synchronous. So if you have some state that is dependent on another state, then is better to change that state separately of each other.

15.FEB.2021
Inferring - means that the types are going to be automatically detected in programming languages.
VSCode does this inferring for example.

10.FEB.2021
Sparge cat mai mult componentele
Bottom up component creation - construim componentele inainte de screen-uri

In JS the screens are not the native screens that are rendered but instead everything lives in a JS view. Each screen is a JS view that is living somewhere outside of the screen and when a tab is tapped or you somehow navigate to that screen then that screen is brought in front and the others are pushed back. 
This can be solved by using the react-native-screens library which renders the native screens instead of the JS ones but it was not stable when writing this.

9.FEB.2021
An expression is a statement that references one or more variables(b)/values or literals(2) combined with a operator
Ex: a = b * 2

There are two types of compiling:
Interpreted: the code is translated when is run
Compiling: means the code that is run is the code that was translated and is ready to run
JS compiles code on the fly and then it also runs it but that does not mean is interpreted.

compound operators - +=, -=, *=. /=

Types are: string, number, boolean, string. These are representations of a specific type of value

**5.FEB.2021**
What is an API? 
Application Programming Interface
Is the part of a function or module that you use. Whatever happens inside that function is not something that you care about or want to know, you only want to use that function/module to do what you need it to do.

**1.FEB.2021**
Orice este immutable in JS
Higher order functions - returneaza orice
Functie pura: 
Rarely we have 100% functional programming - asta 
Input -> output
getCart() => impure fiind ca acceseaza un APi care poate fi down
If statements = branch-uri
O functie nu este pura daca comunica cu un API exterior
Daca trebuie sa mock-uesti ceva === side effect si device impura functia
Abstractizare - 3 * call sites => 1 abstract. Incepi local so slowly move global cand vezi ca e nevoie de ea in mai multe


**29.IAN.2021**
How to slugiffy a string into a URL?
trim: removes whitespace from both ends of a string
lower-case: converts the string to all lower-case
replace: replaces all instances of match with replacement in a given string
We combine all three functions and we can "slugify" our string.


**29.IAN.2021**
slice v splice - instanta noua si acelasi array

reduce -> nu iau date dinafara
reduce -> atunci cand modifici ceva extern
forEach cat mai rar din cauza ca de obicei foreach parcurge un sir si poate chiar modifica elemente de afara care atunci rupe regula programarii functionale
Foreach practic you break immutable 
Immutable date e mult mai usor sa faci track la datele schimbate. La proiecte mari se schimba datele undeva si e greu sa faci track. Pe immutable poti urmari de unde s-au schimbat. 
Obiectul e pe referinta si atunci cum tot dai obiectul acela mai departe de al clasa la clasa si de la functie la functie, daca apare o problema este mai greu sa o gasesti si poate chiar pe urma sa faci modificarea necesara sa rezolvi problema.
Problema la OOP apare de la inheritance, clasa parinte care e mai abstracta si incepi sa o extinzi si ajungi sa ai niste legaturi ciudate 

Si cu functiile poti ajunge la o complexitate dar faci functii extinse 
Riscul la funcitonal este ca functiile si componentele sa fie cat mai simple si spargi logica cat mai mult posibil. 
In loc sa cresti complexitatea unei functii/componente mai bine faci cod duplicat. 


**28.IAN.2021**
Jest - make a mock in every test module -> use jest.doMock()
You may also need to require() the module after it and remove the global import

**27.IAN.2021**
testing-library https://testing-library.com/docs/ is a UI testing library that contains several functions to test UI components

**21.IAN.2021**
Fix image not shown on iOS 14 https://www.npmjs.com/package/react-native-fix-image 
You need to have a apple developer account.

**21.IAN.2021**
Update Xcode - https://github.com/xcpretty/xcode-install/

**21.IAN.2021**
npx vs npm
npx - if you have a package that you have uploaded to npm and you made it executable (there are ways to make your package executable) then you can directly run that package without needing to install it first 
Ex: npm react-native-fix-image
npm -  is a package manager. A free software registy

**20.IAN.2021**
The purpose of a build system is to make sure that the code that ends up in your build meets the standards that your team has agreed upon.
Jenkins - is  free build sistem that you need to install it on a machine that you also need to maintain
Appcenter - is a payd build sistem $40/month that is a cloud solution that you only need to setup once 
GitHub workflows - offers you a hybrid cloud solution to create tests and builds.

**16.IAN.2021**
charCodeAt() - Returns the string character as a number. For example for the string “abc” a = 1, b = 2, c = 3


**15.IAN.2021**
Dayjs - locale can be set for global and locale. For both you need to import the desired language pack like this import 'dayjs/locale/ar';
Globale - you only need to set the locale once in your function like dayjs().locale(‘ar’);
Local - you need to set the locale everytime dayjs(date).locale(‘ar’).format(..).

**14.IAN.2021**
Dayjs - in order to add a text like `at` between the formatting add like this
Ex: dayjs(newDate).format(`DD[/]MM[/]YY [${i18n.t('at')}] HH:mm`);

**14.IAN.2021**
Dayjs is a smaller library then moment and is used to format and display date. 

**07.IAN.2021**
Whatever input format you give to a Date(); It will format it to a general format.
Ex: new Date('2020-12-01') === 2020-12-01T00:00:00.000Z


**06.IAN.2021**
How to add the suffixes for dates like st, nd, th, rd
https://stackoverflow.com/questions/13627308/add-st-nd-rd-and-th-ordinal-suffix-to-a-number

**06.IAN.2021**
Article that shows all the possible formatting types for date in moment.js
https://thecodebarbarian.com/formatting-javascript-dates-with-moment-js.html

**06.IAN.2021**
Article that shows all the possible parameters for toLocalString
https://masteringjs.io/tutorials/fundamentals/date_format

**06.IAN.2021**
Jest mock - what it does is that it takes the initial value of a function or a parameter and then it modifies it with the values given to it by the mock. After that it returns the initial value to the changed function or parameter.

**04.IAN.2021**
Difference between ?? and || 
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator

|| returneaza dreapta in cazul in care stanga este falsy (null, undefined, ‘’, 0)
?? returneaza dreapta in cazul in care stanga este specific null sau undefined si atunci in cazul nostru este mai bine sa folosim ?? fiind ca e caz specific in care verificam sa fie null sau undefined

**04.IAN.2021**
Math.floor - rounds the number downwards
Math.ceil - rounds the number upwards

**10.DEC.2020**
ToLocalDateString
It can return you a date format based on a specfic locale that you can provide to it
PS. For the day value you can also set the day option as “2-digit” if you want ot display the leading 0 as well

**10.DEC.2020**
Current Timestamp
To get that you need to new Date().
In seconds - Math.floor(Date.now() / 1000);
https://stackoverflow.com/questions/221294/how-do-you-get-a-timestamp-in-javascript

**9.DEC.2020**
Formating date
In order to format date you need to extract the data that you want from the date and then create a string out of it.

**9.DEC.2020**
toISOString() - returns a date as string in ISO format which is 24 or 27 characters long and the timezone is always 0 UTC

**8.DEC.2020**
When you do a workaround make sure to specify it in the Changelog:
Motivation (can be a link to an issue)
When to remove it

**8.DEC.2020**
When you do a workaround make sure to leave a comment with the:
Motivation (can be a link to an issue)
When to remove it in the future

**8.DEC.2020**
On iOS if you give a style conditionally like so:
style={{!isIOS && {color: ‘red’}} 
In this way on android it will show the color red but on iOS the style will be false.

**7.DEC.2020**
Ideally a refactor/reafactoring should go like this:
if you don't have tests, write tests for existing implementation
refactor implementation (one method at a time), making sure all tests still pass
spre exemplu:
am metoda isLessThan: (date, options = {}) => {}
pt implementarea curenta, scriu un test
rulez testul, it passes
re-implementez metoda (in cazul nostru, scap de moment)
rulez testul din nou si ma asigur ca trece in continuare


**23.NOV.2020**
You don’t need to spread a string in order to iterate trough it. You can iterate trough it BUT you cannot mutate it.

**23.NOV.2020**
In order to split a string you can also do it like this:
const s = ‘sdsdsd’ 
[...s]
Or you can s.split(“”);

**19.NOV.2020**
Geolocation - is when longitude and latitude is returned from the users current location
Geocoding - when an address is given and coordinates are returned instead
Reverse geocoding - when human readable address is returned after coordinates (lat, long) have been provided

**18.NOV.2020**
Redux Saga 
Call:
 is used only when you need to call a specific function that you have created a saga and/or redux 
You need some data returned from them
Put: 
You call put when you need to put some data in redux. Let’s say you have some saga that has some logic inside of it, you call put not call.
16.NOV.2020
Dependency cycle - is when you have two files A and B and file A calls a function from file B and vice versa. This can cause bugs in the long term so make sure that the resources that A needs are put in a separate file C (for example) and there 

**11.NOV.2020**
In order to edit the Info.plist elements use the plutil tool 
In order to edit the android xml files use the sed tool

**6.NOV.2020**
ReactContext - helps you create a redux kind of structure for your specific component. The difference between redux and ReactContext is that this is only component based, redux is app based.

**6.NOV.2020**
UI State vs APP State 
UI State - is only component based, for example when you have a input field, the UI is only for that specific input
App state - is redux, mobex, useReducer

**6.NOV.2020**
Higher order components receive a component that they will render inside of them but first they run some specific things inside of them and only after that they run the component that they were given.

**6.NOV.2020**
Redux saga:
Take: asculta dupa un action si odata ce acel action a fost chemat el atunci apeleaza un saga specific
Call: va chema o functie specifica si ii face wrap in generator function
Put: efectiv pune niste date, de exemplu atunci cand chemam un succes pentru o actiune

**6.NOV.2020**
React race conditions - they are conditions that are called after another condition is called and you either lose the data from the first or you confuse the user.
https://wanago.io/2020/03/02/race-conditions-in-react-and-beyond-a-race-condition-guard-with-typescript/

**6.NOV.2020**
Regex concatenation - you can concatenate multiple regex conditions into one single condition
For example: [a-z,0-9]+

**4.NOV.2020**
https://github.com/6thstreetdotcom/mobile-app/pull/1585/files
nu inteleg AppRegistry.registerComponent('app6thstreet', () => withCodePush(Setup));
nu inteleg const CodePushContext = React.createContext({});
Contextul se ataseaza de Root e ca si un reducer
ar merge pe ideea de use context si useReducer
Redux-ul e pentru toate componentele din app
Context-ul este granular, doar pentru o singura componenta gen
Faci un context atunci cand nu vrei sa faci un reducer si actions, etc pentru acel specific lucru
https://github.com/6thstreetdotcom/mobile-app/pull/1657/files
js/components/Banners/Vue/withVueDataSource.js
withVueDataSource este un higher order component, prima data se apeleaza chestiile dinauntrul ei si pe urma vine apelata componenta din ea.
Nu inteleg mai exact din saga ce face take si diferenta dintr-e put si call.
redux functioneaza pe actiuni, cineva ia datele si le rezolva. Asta face redux, take 
put -> Pune date undeva, gen face dispatch la un action success
call  -> apeleaza o alta metoda, wrapuieste metoda intr-un generator 


**23.NOV.2020**
Problem solving + functional programming

Cazuri de testat + de acoperit:
nu ai de unde sa stii care sunt cazurile de fiecare data, unele cazuri se descopera pe parcurs
TAKE YOUR TIME
dupa ce ai implementat un tichet, fac o pauza si intoarcete la ea si testeaza. Atunci vei testa cu alti ochi practic si asta te va ajuta sa gasesti noi cazuri.

Reduce - poate fi foarte versatil, il folosesti cand ai immutable data si vrei sa modifici ceva. 

Sa intelegi recursivitatea. E ok sa gasesti tu solutii. Atunci cand rezolvi o problema sa fie atat de clar si fluid incat cand te intorci dupa un an sa intelegi ce se intampla. fati pe foaie prima data sa intelegi.

Reduce are initial value, accumulator va prelua initial value sau primul element din array

Foreach NU e folosit pentru immutable data, pentru asta e folosit Reduce. ForeEach folosesti 
pentru simple parcurgeri dar nu parcurgeri care necesita ca sa modifici niste date.

Fa diferenta dintr-e functional programming si OOP, la functional trebuie sa fi atent sa nu se modifice variabile din exterior

Fi atent la o problema ce iti cere sa returnezi, ce tipuri de date vei primi, etc

In fata unui string o sa converteasca acel string in number.
Ex:  +(string1) 

Am observat ca in unele locuri in app o functie primeste prin argumente anumite date sau ia inuntrul ei din props prin deconstructie. Care este metoda pe care ar trebui sa o folosim?
functia inauntrul ei trebuie sa se ocupe de asta



Configs
E bine sa ai un proiect unde faci un priect si ii faci toate configurarile de eslint, prittier, etc ca sa stii cand incepi un proiect si sa ai toate config-urile
Cam la 1 sau 2 ani trebuie sa le schimbi sau sa le modifici/updatezi
Nameing convention
Deciziile luate pe proiect
Fisiere si Componente - Pascal case
Camel case in JS in general 
Pentru constante, majuscule cu snake case
Utils 
Faci functii acolo cand stii ca o sa fie refolosifbil
Si sa se si vada mai bine ceeace face acea functie de exemplu - daca vrei sa verifici sa fie function
Putem sa spunem ca este un good practice sa punem fiecare functie intr-un useCallback?
Deci pune doar in useCallback doar dacă stii ca se trece de mai multe ori prin niste date, 
Renderul tree de fiecare data -> merge de la partine in jos si ruleaza de fiecare data
Pui functii functionale adica nu au legatura la props-uri, nu fac request nici state-ul nu il modifica, nu sunt specifice pentru componenta respectiva, poti sa iei functia si sa o refolosesti si orce altceva ce nu se foloseste de state si de props
Care este defapt utilitatea la useRef?
Poti sa il folosesti pentru orice ce poate sa ramana intr-e renderuri, ceva ce nu e afectat de render. 
Daca ai o variabila pe care o creezi
Is a refence to something that will not change between renders
BigPicture
Nu il ai la inceput, banuiesti
Gandestete cam ce ar putea fi afectat de atask-ul tau
Ce legaturi exista intr-e ce/unde
La ce ai folosi useContext mai exact si cum mai exact?
Faci share la state intr-e componente
CodePush
Codul de JS care nu are legatura cu partea nativa (bundle)
Daca se modifica si partea nativa atunci iti trebuie defapt code push 
SideEffect
Sa se execute higher in ierarhia componentelor
Ceva ce se intampla ce nu vine de la developer 
Request-ul este un side-effect fiind ca tu nu stii ce primesti
Un field de input, il creezi dar nu stii ce vei primi
PR Revies
Sa nu dai approve decat atunci cand ai 100% incredere ca e corect 
Mocks de api
Nu sunt altceva decat forma unui raspuns si request


**16.SEP.2020**
Incearca de fiecare data sa returnezi același tip de date dintr-o funcție.
Functia sa aiba un return default value.
Daca ai un throw - nu e incalcata regula de a returna acelasi tip de date, depinde cum vrei sa se comporte acea functie
      a. return output only on valid input, throw otherwise
      b. always return an output; when input is not valid, return a default



**16.SEP.2020**
Promise.catch() - can catch all kinds of explicit errors or syntax errors, network errors and such.
https://javascript.info/promise-error-handling#:~:text=catch%20%22%20around%20the%20executor%20automatically,to%20the%20nearest%20error%20handler 

**8.SEP.2020**
Ca sa faci encode la un URL poti sa folosesti https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/encodeURIComponent
Sau
https://www.urlencoder.org/
Fiind ca altfel nu o sa iti ia caracterele asa cum trebuie

**7.SEP.2020**
Single Responsibility Principle - Funcțiile au un singur motiv pentru a se schimba asta inseamna ca ceeace ar trebui sa returneze o functie trebuie sa fie acelasi tip de date, oricate verificari face, ea trebuie sa returneze acelasi lucru. 
