# Overview

On this repository you can find my PDP. 
Everything that I have learned and all the exercises that I have made from 01.09.2020 untill now.
Once the review period is over, I will organise the information found here in periods of 6 months so that for the next period 
the reviewer will know which one is the relevant information on my PDP.


## What and where

In the Exercises folder you will find the exercises that I do on a regular basis that help me understand some concepts and tools
that are needed in my everyday job.
In the Documents folder you will find the new knowaledge that I learn on a regular basis.

## Other resources

There are other documents as well that I have where I write down knowaledge that acquire of the project that I work on or 
on other tools, javascript, etc.
You can access these links only with a yopeso email address.

### Project documentation

Documentation written for the project as I continuously discovered new things:
https://docs.google.com/document/d/1duSBRWRuq-m5K4aNNfbqgCq7jReKQ5SgH60Grr_tQ1g/edit?usp=sharing

### Project issues & solutions

Documentation of the issues that I had with some tickets and the solutions that was chosen at the end:
https://docs.google.com/document/d/1JHSpNZYCv3Cv61vD9AHvx8WgFFE3TAt78mgz59b4RPw/edit?usp=sharing

### Javascript specific knowaledge

This document I constantly update with javascript specific knowaledge that I acquire constantly:
https://docs.google.com/document/d/1rZ3RJTsNbphysNNkCvCx9vyzaw0PRe1Tjf7ad3AIh5A/edit?usp=sharing


### Functional programming

Here I wrote down everything I know about this topic. I also received feedback from my tech lead Calin Tamas and added the new knowaledge I aquired after the feedback. 

https://docs.google.com/document/d/1jZc-I0QidiCKvdoWHgDTOLMKlnPtZoTJ6j5Zl8Tqgds/edit?usp=sharing 


### You don't know Javascript

Insights, questions and answers that I get upon reading this book
https://github.com/getify/You-Dont-Know-JS

I have started reading it from around the middle of February 2021 and here are my notes:
https://docs.google.com/document/d/1XNkVZzokp09yv5osw62RAtqcY2BRhrffLAqc13cwa2g/edit?usp=sharing
